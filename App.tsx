import {SafeAreaView, StatusBar} from 'react-native';
import SQLApp from './src/screens/UserInfo';
const App = () => {
  return (
    <SafeAreaView style={{backgroundColor: 'white', flex: 1}}>
      <StatusBar
        backgroundColor={'#50052A'}
        animated
        showHideTransition="slide"
        barStyle={'dark-content'}
      />
      <SQLApp />
    </SafeAreaView>
  );
};
export default App;
