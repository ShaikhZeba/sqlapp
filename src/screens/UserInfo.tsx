import React, {useRef, useEffect, useState} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  TextInput,
  FlatList,
  Modal,
  Alert,
  Keyboard,
} from 'react-native';
import styles from './UserInfo.styles';
import DatePicker from 'react-native-date-picker';
import moment from 'moment';
import {
  saveUserItems,
  getDBConnection,
  createTable,
  getUserItems,
  deleteUser,
  editUser,
} from '../Utils/db';
export default function UserInfo() {
  const searchRef = useRef();
  const [search, setSearch] = useState(null);
  const [userObj, setUserObj] = useState({
    name: null,
    mobile: null,
    dob: null,
    userId: null,
  });
  const [isDatePickerVisible, setDatePickerVisible] = useState(false);
  const [userInfo, setUserInfo] = useState([]);
  const [modalVisible, setModalVisible] = useState(false);

  const initDb = async () => {
    const db = await getDBConnection();
    await createTable(db);
    getListOfUser();
  };

  useEffect(() => {
    initDb();
  }, []);
  const saveDetails = async () => {
    if (!userObj.name || !userObj.mobile || !userObj.dob) {
      alert('Please enter details first');
    } else {
      setModalVisible(false);
      const db = await getDBConnection();
      let result;
      if (userObj.userId) {
        result = await editItem(userObj);
      } else {
        result = await saveUserItems(db, userObj);
      }
      if (result) {
        getListOfUser();
      }
    }
  };

  const getListOfUser = async (value?: any) => {
    const db = await getDBConnection();
    let details = await getUserItems(db, value);
    setUserInfo(details);
  };
  const deleteItem = async (id: number) => {
    try {
      const db = await getDBConnection();
      await deleteUser(db, id);
      getListOfUser();
    } catch (error) {
      console.error(error);
    }
  };

  const editItem = async userData => {
    try {
      const db = await getDBConnection();
      await editUser(db, userData);
      getListOfUser();
    } catch (error) {
      console.error(error);
    }
  };

  const emptyComponent = () => {
    return (
      <TouchableOpacity
        onPress={() => {
          setUserObj({});
          setModalVisible(true);
        }}
        style={styles.emptyContainer}>
        <Text
          style={
            styles.text
          }>{`op's No Record Found\nPlease click to enter details`}</Text>
      </TouchableOpacity>
    );
  };
  const renderItem = (item: string) => {
    return (
      <View>
        <View style={styles.cardContainer}>
          <Text style={styles.textStyle}>{`ID: ${item.item.userId}`}</Text>
          <Text style={styles.textStyle}>{`Name: ${item.item.name}`}</Text>
          <Text style={styles.textStyle}>{`Mobile: ${item.item.mobile}`}</Text>
          <Text style={styles.textStyle}>
            {`DOB: ${moment(item.item.dob).format('MMMM D, YYYY')}`}
          </Text>
          <View style={styles.buttonContainer}>
            <TouchableOpacity
              onPress={() => {
                setUserObj(item.item);
                setModalVisible(true);
              }}
              style={styles.touchView}>
              <Text style={styles.buttonText}>{`Edit`}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                Alert.alert(
                  'Delete Info',
                  'Are you sure, you want to delete?',
                  [
                    {
                      text: 'Cancel',
                      onPress: () => console.log('Cancel Pressed'),
                      style: 'cancel',
                    },
                    {text: 'OK', onPress: () => deleteItem(item.item.userId)},
                  ],
                );
              }}
              style={styles.touchView2}>
              <Text style={styles.buttonText}>{`Delete`}</Text>
            </TouchableOpacity>
          </View>
        </View>
      </View>
    );
  };
  return (
    <View style={styles.rootView}>
      <View style={styles.headerMain}>
        <View
          style={styles.headerTitle}>
          <Text
            style={styles.title}>{`SQLlite Application`}</Text>
        </View>
        <View style={styles.headerButton}>
          <TouchableOpacity
            style={styles.addInfoStyles}
            onPress={() => {
              setUserObj({});
              setModalVisible(true);
            }}>
            <Text style={styles.addInfoText}>{`Add Info`}</Text>
          </TouchableOpacity>
        </View>
      </View>
      <TextInput
        ref={searchRef}
        style={styles.inputStyles}
        onChangeText={filterValue => getListOfUser(filterValue)}
        value={search}
        placeholderTextColor={'rgba(0, 0, 0, 0.4)'}
        underlineColorAndroid="transparent"
        placeholder={'Search'}
      />
      <FlatList
        data={userInfo}
        showsVerticalScrollIndicator={false}
        keyExtractor={item => item.userId}
        ListEmptyComponent={emptyComponent}
        renderItem={renderItem}
        contentContainerStyle={{flex: 1}}
      />
      <Modal
        animationType="slide"
        visible={modalVisible}
        onRequestClose={() => {
          setUserObj({});
          setModalVisible(!modalVisible);
        }}>
        <View style={styles.modelView}>
          <View style={styles.modelHeader}>
            <Text style={styles.headerText}>{`User Info`}</Text>
            <TouchableOpacity onPress={() => setModalVisible(false)}>
              <Text style={styles.headerText}>{`Close`}</Text>
            </TouchableOpacity>
          </View>
          <TextInput
            style={styles.inputStyles}
            onChangeText={name =>
              setUserObj({
                ...userObj,
                name: name,
              })
            }
            value={userObj.name}
            placeholderTextColor={'rgba(0, 0, 0, 0.4)'}
            underlineColorAndroid="transparent"
            placeholder={'Name*'}
          />
          <TextInput
            style={styles.inputStyles}
            onChangeText={mobile =>
              setUserObj({
                ...userObj,
                mobile,
              })
            }
            value={userObj.mobile}
            placeholderTextColor={'rgba(0, 0, 0, 0.4)'}
            underlineColorAndroid="transparent"
            keyboardType="numeric"
            placeholder={'Mobile*'}
            maxLength={10}
          />
          <TouchableOpacity
            style={styles.dobContainer}
            onPress={() => {
              Keyboard.dismiss();
              setDatePickerVisible(true);
            }}>
            <Text>
              {userObj.dob
                ? moment(userObj.dob).format('MMMM D, YYYY')
                : 'Date Of birth*'}
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            onPress={() => {
              saveDetails();
            }}
            style={styles.buttonView}>
            <Text style={styles.saveText}>
              {userObj?.userId ? 'Update' : 'Save'}
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
      {isDatePickerVisible && (
        <DatePicker
          textColor="#000"
          modal
          mode="date"
          open={isDatePickerVisible}
          date={new Date()}
          onConfirm={date => {
            setDatePickerVisible(false);
            setUserObj({...userObj, dob: new Date(date).setUTCHours(0, 0, 0)});
          }}
          onCancel={() => {
            setDatePickerVisible(false);
          }}
          maximumDate={new Date(moment().add(-18, 'years'))}
          minimumDate={new Date(moment().subtract('200', 'years'))}
        />
      )}
    </View>
  );
}
