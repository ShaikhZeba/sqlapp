import {StyleSheet} from 'react-native';
const styles = StyleSheet.create({
  inputStyles: {
    borderWidth: 1,
    borderRadius: 10,
    margin: 10,
    padding: 10,
    borderColor: '#707070',
  },
  text: {
    textAlign: 'center',
    color: '#91265B',
    fontSize: 16,
  },
  emptyContainer: {flex: 1, justifyContent: 'center', alignItems: 'center'},
  cardContainer: {margin: 10, padding: 10, borderWidth: 1, borderRadius: 10},
  textStyle: {marginTop: 10},
  buttonContainer: {
    flexDirection: 'row',
    marginVertical: 10,
  },
  touchView: {
    backgroundColor: '#91265B',
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    padding: 10,
  },
  buttonText: {color: '#fff', fontSize: 16},
  touchView2: {
    backgroundColor: '#D5689E',
    alignItems: 'center',
    flex: 1,
    justifyContent: 'center',
    padding: 10,
  },
  rootView: {flex: 1,marginTop:10},
  headerButton: {
    justifyContent: 'flex-end',
    alignItems: 'flex-end',
    flexDirection: 'row',
    padding: 5,
  },
  addInfoStyles: {
    borderWidth: 1,
    padding: 2,
    borderRadius: 10,
    backgroundColor: '#610C36',
    borderColor: '#610C36',
  },
  addInfoText: {fontSize: 18, color: '#fff', margin: 10},
  modelView: {
    marginTop: 10,
    flex: 1,
  },
  modelHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    margin: 10,
    padding: 10,
  },
  headerText: {color: '#610C36', fontSize: 21},
  dobContainer: {
    borderWidth: 1,
    borderRadius: 10,
    margin: 10,
    padding: 10,
    borderColor: '#707070',
    height: 45,
  },
  buttonView: {
    alignItems: 'center',
    borderWidth: 1,
    borderRadius: 10,
    margin: 10,
    padding: 10,
    backgroundColor: '#610C36',
  },
  saveText: {color: '#fff', fontSize: 18, borderColor: '#610C36'},
  headerMain:{flexDirection: 'row', justifyContent: 'space-between'},
  headerTitle:{
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 10,
  },title:{
    color: '#610C36',
    fontSize: 21,
  }
});
export default styles;
