import {
  openDatabase,
  enablePromise,
  SQLiteDatabase,
} from 'react-native-sqlite-storage';
const tableName = 'mst_User';
enablePromise(true);


const checkDate = date => {
  try {
    return new Date(date).getTime();
  } catch (err) {
    return false;
  }
};

export const getDBConnection = async () => {
  return openDatabase({name: 'User.db', location: 'default'});
};
export const createTable = async (db: SQLiteDatabase) => {
  // create table if not exists
  const query = `CREATE TABLE IF NOT EXISTS ${tableName}(
    userId INTEGER PRIMARY KEY AUTOINCREMENT, name VARCHAR(30), mobile VARCHAR(15), dob NUMERIC
);`;

  await db.executeSql(query);
};
export const saveUserItems = async (db: SQLiteDatabase, userDetail: any) => {
  const insertQuery = `INSERT OR REPLACE INTO ${tableName}(name,mobile,dob) values ('${userDetail.name}','${userDetail.mobile}','${userDetail.dob}')`;
  return db.executeSql(insertQuery);
};

export const getUserItems = async (db: SQLiteDatabase, filters?: any) => {
  try {
    const userItems = [];
    let query = `Select * from ${tableName}`;
    if (filters) {
      query += ` where name like '${filters}%' or mobile like '${filters}%'`;
      let filterDate = checkDate(filters);
      if (filterDate) query += ` or dob  == ${filterDate}`;
    }
    console.log('queryqueryquery', query);

    const results = await db.executeSql(query);
    results.forEach(result => {
      for (let index = 0; index < result.rows.length; index++) {
        userItems.push(result.rows.item(index));
      }
    });
    console.log('userItemsuserItems', userItems);

    return userItems;
  } catch (error) {
    console.error(error);
    throw Error('Failed to get todoItems !!!');
  }
};

export const deleteUser = async (db, userId) => {
  try {
    const deleteQuery = `DELETE from ${tableName} where userId = ${userId}`;
    return db.executeSql(deleteQuery);
  } catch (error) {
    return false;
  }
};

export const editUser = async (db, data) => {
  try {
    const query = `UPDATE ${tableName}
      SET name = '${data.name}', mobile= '${data.mobile}',dob= ${data.dob}
      WHERE userId = '${data.userId}';`;
    return db.executeSql(query);
  } catch (error) {
    console.log('Error is', error);
    return false;
  }
};